catalogPath=/pnfs/iihe/cms/store/user/hanwen/DileptonUL/2023-09-11_2017-NanoAODv9/DDF/Dilepton

DoubleEG.yaml
DoubleMuon.yaml
SingleElectron.yaml
SingleMuon.yaml
MuonEG.yaml

DYJetsToLL.yaml
DYJetsToLL_M50_HT100to200.yaml
DYJetsToLL_M50_HT200to400.yaml
DYJetsToLL_M50_HT400to600.yaml
DYJetsToLL_M50_HT600to800.yaml
DYJetsToLL_M50_HT800to1200.yaml
DYJetsToLL_M50_HT1200to2500.yaml
DYJetsToLL_M50_HT2500toInf.yaml
DYJetsToLL-amcatnlo.yaml
DYJetsToLL_PtZ-0To50.yaml
DYJetsToLL_PtZ-50To100.yaml
DYJetsToLL_PtZ-100To250.yaml
DYJetsToLL_PtZ-250To400.yaml
DYJetsToLL_PtZ-400To650.yaml
DYJetsToLL_PtZ-650ToInf.yaml

GluGluToContinToZZTo2e2nu.yaml
GluGluToContinToZZTo2mu2nu.yaml
GluGluToWWToENEN.yaml
GluGluToWWToMNMN.yaml

TTTo2L2Nu.yaml

ST_s-channel.yaml
ST_t-channel_antitop.yaml
ST_t-channel_antitop_4f.yaml
ST_t-channel_top.yaml
ST_t-channel_top_4f.yaml
ST_tW_top.yaml

TTWJetsToLNu.yaml
TTZToLLNuNu_M-10.yaml
tZq_ll.yaml

WWTo1L1Nu2Q.yaml
WWTo2L2Nu.yaml
WZTo1L1Nu2Q.yaml
WZTo1L3Nu.yaml
WZTo2Q2L.yaml
WZTo3LNu.yaml
ZZTo2L2Nu.yaml
ZZTo2Q2L.yaml
ZZTo4L.yaml

WWW.yaml
WWZ.yaml
WZZ.yaml
ZZZ.yaml

VBS_ZZTo2L2Nu.yaml
